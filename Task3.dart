class WinningApp {
  String name;
  String category;
  String developer;
  int year;
  
  //copy constructor
  WinningApp(this.name, this.category, this.developer,this.year);
  
  //getters
  
  String get getName {
   return name;
  }
  
  String get getCategory {
    return category;
  }
    
  String get getDeveloper {
    return developer;
    
  }
   
  int get getYear {
    return year;
  }
  
  
  //setters
  set setName(String appName) {
    name = appName;
  }
  
  set setCategory(String appcategory) {
    category = appcategory;
  }
    
  set setDeveloper(String appDeveloper) {
    developer = appDeveloper;
    
  }
   
  set setYear(int yearDeveloped){
    year = yearDeveloped;
  }
  
  // methods
  
  void printApp(){
    print("App Name: $name");
    print("App Category: $category");
    print("App Developer: $developer");
    print("App Year: $year");
    print("");
  }
  
  void capitalise(){
    String name = this.name;
    print(name.toUpperCase());
  }
  
}

void main() {
  
  
  print("a)");
  var app1 = WinningApp("FNB", "bank","FNB Banking app",2012);
  app1.printApp();
  
  var app2 = WinningApp("SnapScan", "Payment getway Company ","mobile payment solutions app",2013);
  app2.printApp();
  
   var app3 = WinningApp("Live Inspect", "Solution Company","Data capture app",2014);
  app3.printApp();
  
  var app4 = WinningApp("WumDrop ", "logistic company","an on-demand courier service",2015);
  app4.printApp();
  
   var app5 = WinningApp("Domestly ", "cleaning service","cleaning-on-demand startup",2016);
  app5.printApp();
  
  var app6= WinningApp("Standard Bank and Shift", "Money app service ","Digital wallet that allows customers to handle all their forex needs from their phone, with no need to visit the bank.",2017);
  app6.printApp();
  
   var app7 = WinningApp("Khula ecosystem ", " supply chain"," connects emerging farmers to the formal marketplace",2018);
  app7.printApp();
  
  var app8 = WinningApp("Naked Insurance", "Insuretec Startupy ","transforming car insurance from a grudge purchase into a transparent, convenient and simple customer experience",2019);
  app8.printApp();
  
   var app9 = WinningApp("EasyEquities ", " share trading company "," a shares investment app that recently partnered with Capitec to make buying shares easier.",2020);
  app9.printApp();
  
  var app10 = WinningApp("Ambani Africa", "technology company ","gamified african language learning app",2021);
  app10.printApp();
  
  print("b)");
  
  app1.capitalise();
  app2.capitalise();
  app3.capitalise();
  app4.capitalise();
  app5.capitalise();
  app6.capitalise();
  app7.capitalise();
  app8.capitalise();
  app9.capitalise();
  app10.capitalise();
  
}